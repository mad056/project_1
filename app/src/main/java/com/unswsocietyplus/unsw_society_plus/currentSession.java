package com.unswsocietyplus.unsw_society_plus;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.SimpleAdapter;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Joson on 21/04/2018.
 */

public class currentSession {
    public static FirebaseFirestore db;

    currentSession(){

    }
    public static student currentUser;

    public static ArrayList<event> studentEvents;
    public static student viewingStudent;
    public static LatLng selectedLoc = new LatLng(-33.916597, 151.231689);

    public static ArrayList studentSocieties = new ArrayList<society>();
    public static ArrayList studentAnnouncements = new ArrayList<announcement>();
    public static ArrayList studentVoteEvents = new ArrayList<voteEvent>();
    public static event editingEvent = new event();
    public static List<String> societiesArray;
    public static ArrayList<String> jsocietiesArray;
    public static List<String> executiveArray;
    public static String FirstName;
    public static String LastName;
    public static String address;


    public static CollectionReference getRef(String collection){
        return db.collection(collection);
    }

    public static void loadEvents(String societyID){
        db = FirebaseFirestore.getInstance();
        Query query = getRef("Events").whereEqualTo("Society", societyID);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()) {
//                    currentSession.studentEvents.clear();
                    for (DocumentSnapshot document : task.getResult()) {
                        event newEvent = new event();
                        newEvent.setEventID(document.getString("EventID"));
                        newEvent.setEventTitle(document.getString("EventTitle"));
                        newEvent.setEventDescription(document.getString("EventDescription"));
                        newEvent.setDate(document.getString("Date"));
                        newEvent.setTime(document.getString("Time"));
                        newEvent.setLocation(document.getString("Location"));
                        newEvent.setAddress(document.getString("Address"));
                        newEvent.setSocietyID(document.getString("Society"));
                        newEvent.setFbLink(document.getString("fbLink"));
                        newEvent.setDocID(document.getId());
                        currentSession.studentEvents.add(newEvent);



                    }
                    System.out.println("task");
                    System.out.println("currentSession.studentEvents.size() " + currentSession.studentEvents.size());

                } else {
                    System.out.println("DebugErr");
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });
    }

    public static void zidToZmail(){
        currentUser.zmail =  "z" + currentUser.zid + "@student.unsw.edu.au";
    }

    public static void signout(){
        currentUser = null;
        studentEvents = null;
    }


    public static student loadStudents(String ZID){
        viewingStudent = null;
        jsocietiesArray = new ArrayList<>();
        societiesArray = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("Student").document(ZID);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        viewingStudent = new student(document.getString("ZID"), document.getString("FirstName") + " " + document.getString("LastName"));
                        viewingStudent.stringExecutiveOf = document.getString("JExecutiveOf");
                        viewingStudent.stringMemberOf = document.getString("JMemberOf");
                        FirstName = document.getString("FirstName");
                        LastName = document.getString("LastName");
                        String stringMemberOf = document.getString("MemberOf");
                        String stringExecutiveOf = document.getString("ExecutiveOf");

                        if (!stringMemberOf.equals(null)) {
                            societiesArray = Arrays.asList(stringMemberOf.substring(1, stringMemberOf.length() - 1).replaceAll("\\s", "").split(","));
                        }


                        if (!stringExecutiveOf.equals(null)) {
                            executiveArray = Arrays.asList(stringExecutiveOf.substring(1, stringExecutiveOf.length() - 1).replaceAll("\\s", "").split(","));

                        }



                        if (!viewingStudent.stringMemberOf.equals("")) {
                            if (viewingStudent.stringMemberOf.contains(",")){
                                String[] stringArrayMember = viewingStudent.stringMemberOf.split(",");
                                System.out.println("stringMemberOf: " + viewingStudent.stringMemberOf);
                                for (int i = 0; i < stringArrayMember.length; i++) {
                                    jsocietiesArray.add(stringArrayMember[i]);
                                }
                            }else{
                                System.out.println("viewingStudent.stringMemberOf: " + viewingStudent.stringMemberOf);
                                jsocietiesArray.add(viewingStudent.stringMemberOf);
                            }
                        }
                        if (!viewingStudent.stringExecutiveOf.equals("")) {
                            if (viewingStudent.stringExecutiveOf.contains(",")){
                                String[] stringArrayExecutive = viewingStudent.stringExecutiveOf.split(",");
                                System.out.println("stringExecutiveOf: " + viewingStudent.stringExecutiveOf);
                                for (int i =0; i<stringArrayExecutive.length; i++){
                                    jsocietiesArray.add(stringArrayExecutive[i]);
                                }
                            }else{
                                jsocietiesArray.add(viewingStudent.stringExecutiveOf);
                            }
                        }
                    }
                } else {
                    System.out.println("DebugErr");
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });
        return viewingStudent;

    }
}
