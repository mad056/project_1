package com.unswsocietyplus.unsw_society_plus;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by chong on 21/03/2018.
 */

public class calenderFragmentBackup extends Fragment {
    View myView;
    private ListView list;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    CalendarView calendarView;
    String pressedDate;
    TextView TextView;
    ListView eventListView;
    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    public static ArrayList<event> eventList;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    ArrayList<event> eventsOnDay= new ArrayList<event>();



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.calendar_layout, container, false);
        getActivity().setTitle("My Calender ");
        return myView;



    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       /* CalendarView calendarView=(CalendarView) myView.findViewById(R.id.calendarView);
        calendarView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
            @Override
        public void onGlobalLayout()
        {
            System.out.println("changed");
        }
        });
*/
        eventListView = myView.findViewById(R.id.eventListView);
        eventList = new ArrayList<event>();
        calendarView = myView.findViewById(R.id.calendar1);
        TextView = myView.findViewById(R.id.textView);
        TextView.setText(sdf.format(Calendar.getInstance().getTime()) + " (Today)");


        FloatingActionButton fab = (FloatingActionButton) myView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View view) {
                createEvent();
                }
        });


        loadEvents();

        //fakedLoadEvents(sdf.format(Calendar.getInstance().getTime()));

     /*   ListAdapter notisficationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_2, events);
        eventListView = myView.findViewById(R.id.eventListView);
        eventListView.setAdapter(notisficationAdapter);
*/
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                month++;
                System.out.println("date changed");
                pressedDate = dayOfMonth + "/" + month + "/" + year;
                System.out.println(sdf.format(Calendar.getInstance().getTime()));
                fakedLoadEvents(pressedDate);
                if (pressedDate.equals(sdf.format(Calendar.getInstance().getTime()))) {
                    pressedDate = pressedDate + " (Today)";
                }
                TextView.setText(pressedDate);

            }
        });

    }

    void loadEvents(String date) {
        System.out.println("events on " + date + ": " + loadEventsByDate(date).size());
    }


    public void loadEvents() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        CollectionReference Ref = db.collection("Events");
        Query query = Ref.whereEqualTo("Society", "10001");

        list = (ListView) myView.findViewById(R.id.eventListView);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String> (getActivity(),R.layout.mylist,arrayList);
        list.setAdapter(adapter);
        registerForContextMenu(list);

        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        event newEvent = new event();
                        newEvent.setEventID(document.getString("EventID"));
                        newEvent.setEventTitle(document.getString("EventTitle"));
                        String title = document.getString("EventTitle");
                        System.out.println(title);
                        newEvent.setEventDescription(document.getString("EventDescription"));
                        newEvent.setDate(document.getString("DateTime"));
                        newEvent.setLocation(document.getString("Location"));
                        arrayList.add(title);
                        adapter.notifyDataSetChanged();
                        eventList.add(newEvent);
                    }
                    System.out.println("task");
                    System.out.println("eventList.size() " + eventList.size());

                } else {
                    System.out.println("DebugErr");
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });
    }

    public ArrayList<event> loadEventsByDate(String date) {
        eventList.clear();
        loadEvents();
        ArrayList<event> eventsOfDay = new ArrayList<event>();
        System.out.println("eventList size: " + eventList.size());
        System.out.println("eventList.size() again1 " + eventList.size());
        for (int i = 0; i < eventList.size(); i++) {
            System.out.println("i = " + i);
            System.out.println(eventList.get(i).date);
            if (eventList.get(i).date.equals(date)) {
                eventsOfDay.add(eventList.get(i));
                System.out.println(eventList.get(i).toString());
            }
        }
        return eventsOfDay;
    }

    public void fakedCreateEvent() {
        System.out.println("eventList.size() " + eventList.size());

        //   eventList.clear();
     //   eventList.add(new event("10000", "BSOC meeting 1", "BSOC meeting description", "8/5/2018", "12:00", "UNSW"));
     //   eventList.add(new event("10001", "BSOC meeting 2", "BSOC meeting description", "8/5/2018", "14:00", "UNSW"));

    }



    public void fakedLoadEvents(String date) {

        eventsOnDay.clear();
        for (int i = 0; i < eventList.size(); i++) {
            System.out.println("i = " + i);
            System.out.println(eventList.get(i).date);
            if (eventList.get(i).date.equals(date)) {
            eventsOnDay.add(eventList.get(i));
            }
        }
        final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
        ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < eventsOnDay.size(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, eventsOnDay.get(i).eventTitle);
            item.put(ID_SUBTITLE,eventsOnDay.get(i).eventDescription);
            myListData.add(item);
        }


        if (myView != null) {
            eventListView.setAdapter(
                    new SimpleAdapter(
                            getActivity(),
                            myListData,
                            android.R.layout.simple_list_item_2,
                            new String[]{ID_TITLE, ID_SUBTITLE},
                            new int[]{android.R.id.text1, android.R.id.text2}
                    )
            );
        }
        eventListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("position: " + position);
                viewEvent(eventsOnDay.get(position));
            }
        });

    }



    public void viewEvent(event event){
        main.viewingEvent = event;
        startActivity(new Intent(getActivity(), viewEvent.class));

    }

    public void createEvent(){
        startActivity(new Intent(getActivity(), createEvent.class));
    }

}

