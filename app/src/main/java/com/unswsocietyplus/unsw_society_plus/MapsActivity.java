package com.unswsocietyplus.unsw_society_plus;

import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentSession.selectedLoc));
        }catch(Exception ex){
            LatLng unsw = new LatLng(-33.916597, 151.231689);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(unsw));
        }
        mMap.setMinZoomPreference(14.0f);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Toast.makeText(MapsActivity.this, "Long press to select your location", Toast.LENGTH_SHORT).show();
            }
        });
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                currentSession.selectedLoc = latLng;
                Toast.makeText(MapsActivity.this, "Press refresh to get your location", Toast.LENGTH_SHORT).show();
                currentSession.address = latLngToString(latLng.latitude, latLng.longitude);
                finish();
            }
        });
    }

    public String latLngToString(Double lat, Double lng) {
        Geocoder geocoder;
        List<Address> addresses;
        try {
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(lat, lng, 1);
            String address = addresses.get(0).getAddressLine(0);
            return address;
        }catch (Exception ex){
            System.out.println(ex);
            return null;
        }
    }
}
