package com.unswsocietyplus.unsw_society_plus;

import android.content.Context;
import android.content.Intent;
import android.icu.text.SymbolTable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class CreateAnnoucement extends AppCompatActivity {
    private ListView list;
    private PopupWindow mPopupWindow;
    private Context mContext;
    private RelativeLayout mRelativeLayout;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    public String answer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_annoucement);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        TextView Society = (TextView)findViewById(R.id.textView36);
        TextView thedate = (TextView)findViewById(R.id.textView40);

        Society.setText(allSocitiesActivity.ChosenAnn);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        String format = simpleDateFormat.format(new Date());
        thedate.setText(format);

    }

    public String getsoc(String des) {
        System.out.println(des);


        // startActivity(new Intent(Vote.this, CreateVote.class));


        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Declare the textviews
String yes = des;

        // this is disgusting. filthy
        // for (String s : messages) {
        CollectionReference StudentRef = db.collection("Societies");
        Query query = StudentRef.whereEqualTo("SocietyName", yes);


        //      Query query = StudentRef.whereEqualTo("VoteName", CreateVote.VoteOption );
   //     System.out.println("Debug 2 :" + CreateVote.VoteOption);

        //    query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {


                        answer = document.getString("SocietyID").toString();




                    }
                } else {
                    System.out.println("DebugErr");

                    Log.d("TAG", "Error getting documents: ", task.getException());
                }

            }
        });
        return answer;
    }
    public void CreateAnn(View view) {

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference cities = db.collection("Announcements");


        EditText description = (EditText)findViewById(R.id.editText13);
        EditText name = (EditText)findViewById(R.id.Yeah);


        Random rand = new Random();
        int randomNum = rand.nextInt((999999 - 1) + 1) + 1;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        String format = simpleDateFormat.format(new Date());

        Map<String, Object> data1 = new HashMap<>();
        data1.put("AnnouncementsAuthor", main.CurrentUser);
        data1.put("AnnouncementsSociety", allSocitiesActivity.ChosenAnn);
        data1.put("AnnouncementsDescription",   description.getText().toString());
        data1.put("AnnouncementsName", name.getText().toString());
        data1.put("AnnouncementsPublished", format);

        cities.document(name.getText().toString()).set(data1);

        Toast.makeText(getApplicationContext(), "Created!", Toast.LENGTH_LONG).show();
        startActivity(new Intent(CreateAnnoucement.this, home.class));
    }

}




