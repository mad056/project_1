package com.unswsocietyplus.unsw_society_plus;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

public class BallotVote extends AppCompatActivity {
    private ListView list;
    private PopupWindow mPopupWindow;
    private Context mContext;

    private RelativeLayout mRelativeLayout;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ballot_vote);


        // startActivity(new Intent(Vote.this, CreateVote.class));
        list = (ListView) findViewById(R.id.List_dynamic);

        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, R.layout.mylist, arrayList);
        list.setAdapter(adapter);
        registerForContextMenu(list);

        // List<String> messages = Arrays.asList("Description", "BITSA", "How", "Are", "Society");
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Declare the textviews
        final TextView soceity = (TextView) findViewById(R.id.society);
        final TextView description = (TextView) findViewById(R.id.description);
        final TextView date = (TextView) findViewById(R.id.Date);
        final TextView name = (TextView) findViewById(R.id.name2);

        // this is disgusting. filthy
        // for (String s : messages) {
        CollectionReference StudentRef = db.collection("VoteEvents");
        DocumentReference docRef = db.collection("VoteEvents").document(CreateVote.VoteOption);

        //      Query query = StudentRef.whereEqualTo("VoteName", CreateVote.VoteOption );
        System.out.println("Debug 2 :" + CreateVote.VoteOption);

        //    query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

            @Override
            //      public void onComplete(@NonNull Task<QuerySnapshot> task) {
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {


                    //   for (DocumentSnapshot document : task.getResult()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {


                        System.out.println("TEST");

                        System.out.println(document.getString("EventName"));
                        name.setText(document.getString("EventName"));
                        date.setText(document.getString("EventDueDate"));
                        description.setText(document.getString("EventDescription"));
                        soceity.setText(document.getString("EventSoceiety"));
                        String options = document.getString("Options");
                        List<String> myList = Arrays.asList(options.substring(1, options.length() - 1).replaceAll("\\s", "").split(","));
                        for (String obj : myList) {
                            arrayList.add(obj);
                            adapter.notifyDataSetChanged();
                        }


                    }
                } else {
                    System.out.println("DebugErr");

                    Log.d("TAG", "Error getting documents: ", task.getException());
                }

            }
        });
        //
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.List_dynamic) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.votevalues);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.votevalues);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);

        //System.out.println("Debug 1 :" + listItemName);
        Toast.makeText(getApplicationContext(), "Thank You For Voting!", Toast.LENGTH_LONG).show();


        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference cities = db.collection("Ballot");



        final TextView name2 = (TextView) findViewById(R.id.name2);


        Random rand = new Random();
        int randomNum = rand.nextInt((999999 - 1) + 1) + 1;


        Map<String, Object> data1 = new HashMap<>();
        data1.put("VoteEventID", name2.getText().toString());
        data1.put("VoteOption", listItemName);
        data1.put("Voter", main.CurrentUser);
       // String key = db.getReference("todoList").push().getKey();
        //data1.put("Options", testString);
        cities.document(Integer.toString(randomNum)).set(data1);

        startActivity(new Intent(BallotVote.this, home.class));

        // Toast.makeText(Vote.this,
        // System.out.println(menuItemName);
        return true;
    }
}
