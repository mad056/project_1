package com.unswsocietyplus.unsw_society_plus;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import static android.view.View.OnClickListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class CreateVote extends AppCompatActivity  implements OnClickListener {
    public static String VoteOption;
    private ImageButton btnAdd;
    private EditText et;
    private ListView lv;
    ArrayList<String> list = new ArrayList<String>();
    ArrayAdapter<String> adapter;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_create_vote);

        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_create_vote);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnAdd = (ImageButton)findViewById(R.id.imageButton);

        btnAdd.setOnClickListener(this);
        et = (EditText)findViewById(R.id.editText);
       adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);

        // set the lv variable to your list in the xml
        lv = (ListView) findViewById(R.id._dynamic);
        lv.setAdapter(adapter);

        //ListAdapter adapter = lv.getAdapter() ;
    }

    public void ApplyBtnClicked(View view) {
     ;//unique name
         ListView lv = (ListView) findViewById(R.id._dynamic);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference cities = db.collection("VoteEvents");

         String[] stringArray = new String[lv.getCount()];

        ListAdapter adapter = lv.getAdapter() ;
        for(int i = 0; i < adapter.getCount(); i++) {
            stringArray[i] = adapter.getItem(i).toString();
        }
        String testString = Arrays.toString(stringArray);

        EditText description = (EditText)findViewById(R.id.editText3);
        EditText name = (EditText)findViewById(R.id.editText4);
        EditText date = (EditText)findViewById(R.id.editText2);
        EditText organiser = (EditText)findViewById(R.id.editText7);
        EditText soceity = (EditText)findViewById(R.id.editText9);

        Random rand = new Random();
        int randomNum = rand.nextInt((999999 - 1) + 1) + 1;


        Map<String, Object> data1 = new HashMap<>();
        data1.put("EventDescription", description.getText().toString());
        data1.put("EventDueDate", date.getText().toString());
        data1.put("EventOrganizer", organiser.getText().toString());
        data1.put("EventSoceiety", soceity.getText().toString());
        data1.put("VoteEventID",   name.getText().toString());
        data1.put("Options", testString);
        data1.put("EventName", name.getText().toString());
        cities.document(name.getText().toString()).set(data1);

        startActivity(new Intent(CreateVote.this, Vote.class));
    }

    public String GetVoteOption() {
        return VoteOption;
    }

    public void onClick(View v)
    {
        String input = et.getText().toString();
        if(input.length() > 0)
        {
            // add string to the adapter, not the listview
            adapter.add(input);
            // no need to call adapter.notifyDataSetChanged(); as it is done by the adapter.add() method
        }
    }
}
