package com.unswsocietyplus.unsw_society_plus;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class myEventsActivity extends AppCompatActivity {
    //FragmentManager fragmentManager = getFragmentManager();
    private TextView mTextMessage;
    Fragment calenderFragment = new calenderFragment();
    Fragment listFragment = new allEventsFragment();
    CalendarView calendarView;
    TextView TextView;
    ListView eventListView;



    public void switchToCalFragment1() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, calenderFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void switchToListFragment1() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, listFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    switchToCalFragment1();




                    return true;
                case R.id.navigation_dashboard:
                    switchToListFragment1();


                    return true;

            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_events);
        eventListView = findViewById(R.id.eventListView);
        calendarView = findViewById(R.id.calendar1);
        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        switchToCalFragment1();
        //loadEvents();
        //loadEventsByDate(t);



    }


    @Override
    public void onBackPressed()
    {
        finish();
    }



    public void loadEvents() {
        for (int i = 0; i < currentSession.currentUser.executiveOf.size(); i++){
            currentSession.loadEvents(currentSession.currentUser.executiveOf.get(i).societyID);
        }
        for (int i = 0; i < currentSession.currentUser.memberOf.size(); i++){
            currentSession.loadEvents(currentSession.currentUser.memberOf.get(i).societyID);
        }
    }

    public void createEvent(){
        startActivity(new Intent(this, createEvent.class));
    }


}
