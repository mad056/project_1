package com.unswsocietyplus.unsw_society_plus;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by chong on 21/03/2018.
 */

public class allEventsFragment extends Fragment{
    View myView;
    ListView allEventsListView;
    final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
    SimpleAdapter adapter;
    ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        myView = inflater.inflate(R.layout.all_events_layout, container,false);
        getActivity().setTitle("All events");
        allEventsListView = myView.findViewById(R.id.allEventsListView);
        adapter = new SimpleAdapter(getActivity(), myListData, android.R.layout.simple_list_item_2, new String[]{ID_TITLE, ID_SUBTITLE}, new int[]{android.R.id.text1, android.R.id.text2});
        loadEvents();



        return myView;
    }

    public void loadEvents() {


        final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
        ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < currentSession.studentEvents.size(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, currentSession.studentEvents.get(i).eventTitle);
            item.put(ID_SUBTITLE,currentSession.studentEvents.get(i).societyID);
            myListData.add(item);
        }


        if (myView != null) {
            allEventsListView.setAdapter(
                    new SimpleAdapter(
                            getActivity(),
                            myListData,
                            android.R.layout.simple_list_item_2,
                            new String[]{ID_TITLE, ID_SUBTITLE},
                            new int[]{android.R.id.text1, android.R.id.text2}
                    )
            );
        }
        allEventsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("position: " + position);
                viewEvent(currentSession.studentEvents.get(position));
            }
        });

    }



    public void viewEvent(event event){
        main.viewingEvent = event;
        startActivity(new Intent(getActivity(), viewEvent.class));

    }


}
