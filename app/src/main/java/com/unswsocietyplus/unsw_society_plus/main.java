package com.unswsocietyplus.unsw_society_plus;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

/**
 * Created by chong on 20/03/2018.
 */

public class main {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("test database");
    String zid;

    public static String CurrentUser;
    public static List<String> currentusersocieties;
    public static event viewingEvent;
    public static society viewingSociety;

    main(){
    }

    boolean login(String zid, String password){
        if (zid.equals("1") && password.equals("1")) {
            return true;
        }else{
            return false;
        }
    }

    //checks: true = all good
    boolean checkEmpty(String text){
        if (text.toString() == "") {
            return false;
        }else{
            return true;
            }
    }

    //check if the String is formed by digits and the digits it has
    boolean checkDigit(String text, int digit){
        char[] ch = text.toCharArray();
        if (ch.length == digit){
            for (int i=0;i<ch.length;i++) {
                if (Character.isDigit(ch[i])==false) {
                    System.out.println("digit check failed");
                    return false;
                }
            }return true;
            }else{
            System.out.println("digit check failed");
            return false;
        }
    }

    void firebaseTesting(){
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                System.out.println(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("Failed to read value."+ error.toException());
            }
        });

    }

    String zmailToZid(String zmail){
        String zid = zmail.split("@")[0];
        StringBuilder stringBuilder = new StringBuilder(zid);
        stringBuilder.deleteCharAt(0);
        return zid;
    }

    String zidToZmail(String zid){
       return "z" + zid + "@student.unsw.edu.au";
    }


}
