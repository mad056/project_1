package com.unswsocietyplus.unsw_society_plus;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.net.URL;

public class viewSocietyActivity extends AppCompatActivity {
    ImageView imageView;
    TextView name;
    TextView description;
    TextView id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_society);

        imageView = findViewById(R.id.imageView);
        name = findViewById(R.id.name);
        description = findViewById(R.id.description);
        id = findViewById(R.id.id);
        try {
            Picasso.with(this)
                    .load(main.viewingSociety.societyIconLink)
                    .into(imageView);

        }catch (Exception ex){
            System.out.println("Failed loading image");
        }
        name.setText(main.viewingSociety.societyName);
        description.setText(main.viewingSociety.societyDescription);
        id.setText(main.viewingSociety.societyID);
    }
}
