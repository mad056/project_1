package com.unswsocietyplus.unsw_society_plus;

/**
 * Created by Joson on 21/04/2018.
 */

public class society {
    String societyID;
    String societyName;
    String societyDescription;
    String societyIconLink;

    society(){

    }

    society(String societyID, String societyName, String societyDescription, String societyIconLink) {
        this.societyID = societyID;
        this.societyDescription = societyDescription;
        this.societyName = societyName;
        this.societyIconLink = societyIconLink;
    }


}
