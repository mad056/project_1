package com.unswsocietyplus.unsw_society_plus;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by chong on 21/03/2018.
 */

public class calenderFragment extends Fragment {
    View myView;
    CalendarView calendarView;
    String pressedDate;
    TextView TextView;
    TextView noEvent;
    ListView eventListView;
    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");
    String todayDate;
    String displayDate;
    boolean fabHidden = true;
    final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
    FloatingActionButton reloadFab;
    FloatingActionButton addFab;

    SimpleAdapter adapter;
    ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();
    ArrayList<event> eventsOnDay = new ArrayList<event>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.calendar_layout, container, false);
        getActivity().setTitle("My Calender ");
        return myView;



    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        noEvent = myView.findViewById(R.id.noEvent);
        eventListView = myView.findViewById(R.id.eventListView);
        calendarView = myView.findViewById(R.id.calendar1);
        TextView = myView.findViewById(R.id.textView);
        todayDate = sdf.format(Calendar.getInstance().getTime());
        TextView.setText(todayDate + " (Today)");
        adapter = new SimpleAdapter(getActivity(), myListData, android.R.layout.simple_list_item_2, new String[]{ID_TITLE, ID_SUBTITLE}, new int[]{android.R.id.text1, android.R.id.text2});


        reloadFab = (FloatingActionButton) myView.findViewById(R.id.reloadFab);
        reloadFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadEvents();
            }
        });
        addFab = (FloatingActionButton) myView.findViewById(R.id.addFab);
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createEvent();
            }
        });


        FloatingActionButton fab = (FloatingActionButton) myView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View view) {
                if (fabHidden){
                    reloadFab.setVisibility(View.VISIBLE);
                    if (currentSession.currentUser.executiveOf.size()>0){
                        addFab.setVisibility(View.VISIBLE);
                    }
                    fabHidden = false;
                }else{
                    reloadFab.setVisibility(View.INVISIBLE);
                    addFab.setVisibility(View.INVISIBLE);
                    fabHidden = true;
                }

                }
        });



        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {

                month++;
                System.out.println("date changed");
                pressedDate = dayOfMonth + "/" + month + "/" + year;
                System.out.println(sdf.format(Calendar.getInstance().getTime()));
                loadEventsByDate(pressedDate);
                //fakedLoadEvents(pressedDate);
                if (pressedDate.equals(todayDate)) {
                    displayDate = pressedDate + " (Today)";
                    TextView.setText(displayDate);
                }else{
                    TextView.setText(pressedDate);
                }


            }
        });

        //loadEvents();
        loadEventsByDate(todayDate);
        System.out.println("Today's date is " + todayDate);
    }



    public void loadEvents() {
        currentSession.studentEvents.clear();
        for (int i = 0; i < currentSession.currentUser.executiveOf.size(); i++){
            currentSession.loadEvents(currentSession.currentUser.executiveOf.get(i).societyID);
        }
        for (int i = 0; i < currentSession.currentUser.memberOf.size(); i++){
            currentSession.loadEvents(currentSession.currentUser.memberOf.get(i).societyID);
        }

        //if(pressedDate != null){
        //    loadEventsByDate(pressedDate);
        //}else{
        //    loadEventsByDate(todayDate);
        //}

    }

    public void loadEventsByDate(String date) {
        myListData.clear();
        adapter.notifyDataSetChanged();
        eventsOnDay.clear();
        //loadEvents();
        System.out.println("Events loading: " + date);


        for (int i = 0; i < currentSession.studentEvents.size(); i++) {
            if (currentSession.studentEvents.get(i).date.equals(date)) {
                eventsOnDay.add(currentSession.studentEvents.get(i));
            }
        }

        for (int i = 0; i < eventsOnDay.size(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, eventsOnDay.get(i).eventTitle);
            item.put(ID_SUBTITLE,eventsOnDay.get(i).societyID);
            myListData.add(item);
        }


        if (myView != null) {
            System.out.println("set");
            eventListView.setAdapter(adapter);
        }
        eventListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("position: " + position);
                viewEvent(eventsOnDay.get(position));
            }
        });
    if (eventsOnDay.size()==0){
        noEvent.setVisibility(View.VISIBLE);
        System.out.println("Size: 0");
    }else{
        noEvent.setVisibility(View.INVISIBLE);
    }
    }

  /*
    public void fakedCreateEvent() {

        System.out.println("eventList.size() " + currentSession.studentEvents.size());

        currentSession.studentEvents.clear();
        currentSession.studentEvents.add(new event("10000", "BSOC meeting 1", "BSOC meeting description", "8/5/2018", "12:00", "UNSW"));
        currentSession.studentEvents.add(new event("10001", "BSOC meeting 2", "BSOC meeting description", "8/5/2018", "14:00", "UNSW"));

    }

   // static public void fakedCreateEvent2(String eventID, String eventTitle, String eventDescription, String date, String time, String location){
   //     currentSession.studentEvents.add(new event(eventID, eventTitle, eventDescription,date, time, location));
   // }
     */
    public void fakedLoadEvents(String date) {

        eventsOnDay.clear();
        for (int i = 0; i < currentSession.studentEvents.size(); i++) {
            System.out.println("i = " + i);
            System.out.println(currentSession.studentEvents.get(i).date);
            if (currentSession.studentEvents.get(i).date.equals(date)) {
            eventsOnDay.add(currentSession.studentEvents.get(i));
            }
        }
        final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
        ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < eventsOnDay.size(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, eventsOnDay.get(i).eventTitle);
            item.put(ID_SUBTITLE,eventsOnDay.get(i).eventDescription);
            myListData.add(item);
        }


        if (myView != null) {
            eventListView.setAdapter(
                    new SimpleAdapter(
                            getActivity(),
                            myListData,
                            android.R.layout.simple_list_item_2,
                            new String[]{ID_TITLE, ID_SUBTITLE},
                            new int[]{android.R.id.text1, android.R.id.text2}
                    )
            );
        }
        eventListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("position: " + position);
                viewEvent(eventsOnDay.get(position));
            }
        });

    }



    public void viewEvent(event event){
        main.viewingEvent = event;
        startActivity(new Intent(getActivity(), viewEvent.class));

    }

    public void createEvent(){
        startActivity(new Intent(getActivity(), createEvent.class));
    }

}

