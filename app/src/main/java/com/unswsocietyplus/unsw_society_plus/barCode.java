package com.unswsocietyplus.unsw_society_plus;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import android.graphics.Color;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class barCode extends AppCompatActivity {

    TextView studentName;
    TextView PassFail;
    private ArrayList<String> arrayList;
    String zidString = "";
    TextView studentSociety;
    TextView studentZID;
    boolean flag = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        arrayList = new ArrayList<String>();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code);
        studentName = findViewById(R.id.studentName);
        PassFail = findViewById(R.id.PassFail);
        studentSociety = findViewById(R.id.studentSociety);
        studentZID = findViewById(R.id.studentZID);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this, new String[] {android.Manifest.permission.CAMERA}, 100);
        }



    }



    public void onSaveBarcode(View v) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference cities = db.collection("Attendance");

        String[] stringArray = new String[arrayList.size()];

        for(int i = 0; i < arrayList.size(); i++) {
            stringArray[i] = arrayList.get(i).toString();
        }
        String testString = Arrays.toString(stringArray);
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());


        Map<String, Object> data1 = new HashMap<>();
        data1.put("List", testString);
        data1.put("Name", timeStamp);
        data1.put("Society", AttendenceScreen.Chosen);

        cities.document(timeStamp).set(data1);

    }


    public void onScanBarcode(View v){
        this.start();

    }

    public void start() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        flag = false;
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setPrompt("Scan student card");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
        studentName.setText("");
        studentSociety.setText("");
        studentZID.setText("");

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Scanning failed", Toast.LENGTH_LONG).show();

            } else {
                String barCodeResult = result.getContents();

                try {
                    String[] barCodeResultArray = barCodeResult.split("");
                    String[] zidArray = new String[7];
                    System.arraycopy(barCodeResultArray, 3, zidArray, 0, 7);

                    for (int i = 0; i <= 6; i++) {
                        zidString += zidArray[i];
                    }
                }catch (Exception ex){
                    System.out.println(ex);
                    Toast.makeText(this, "Incorrect student ID", Toast.LENGTH_LONG).show();
                }


                Toast.makeText(this, "Barcode:  " + barCodeResult, Toast.LENGTH_LONG).show();
                studentZID.setText("ZID: " + zidString);

                     /*
                if (zidString.equals("5036635")){

                    studentName.setText("Student Name: "+"Joson Chong");
                    studentSociety.setText("Society: "+"BSOC");
                }else if (zidString.equals("5021118")) {
                    studentName.setText("Student Name: " + "Preetpal Singh Dhillon");
                    studentSociety.setText("Society: " + "BITSA");
                }

                if (barCodeResult.equals("X1503663511604")){

                    studentName.setText("Student Name: "+"Joson Chong");
                    studentSociety.setText("Society: "+"BSOC");
                }else if (barCodeResult.equals("X1502111811405")){
                    studentName.setText("Student Name: "+"Preetpal Singh Dhillon");
                    studentSociety.setText("Society: "+"BITSA");
                }

        */

                if (main.CurrentUser!=null){
                    studentName.setText(currentSession.FirstName + " " + currentSession.LastName);
                    String societies = "";
                    for (int i = 0; i<currentSession.societiesArray.size(); i++){
                        if (currentSession.societiesArray.get(i).equals(AttendenceScreen.Chosen)){
                            flag = true;
                        }
                        societies += (currentSession.societiesArray.get(i)+ "    ");
                    }
                    studentSociety.setText(societies);
                }
                else{
                    studentName.setText("Student not found");
                }
            }

            if (flag == true)  {
                PassFail.setTextColor(Color.GREEN);
                PassFail.setText("PASS");
                arrayList.add(zidString);
            }
            if (flag == false)  {
                PassFail.setTextColor(Color.RED);
                PassFail.setText("FAIL");
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }





    }
}

/*
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;

import java.util.logging.Logger;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class barCode extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        // Programmatically initialize the scanner view
        mScannerView = new ZXingScannerView(this);
        // Set the scanner view as the content view
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);
        // Start camera on resume
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        // Stop camera on pause
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        // Prints scan results
        String code = rawResult.getText();
        Log.d("result", code);
        // Prints the scan format (qrcode, pdf417 etc.)
        String formate = rawResult.getBarcodeFormat().toString();
        Log.d("result", formate);
        //If you would like to resume scanning, call this method below:
        //mScannerView.resumeCameraPreview(this);
        //Intent intent = new Intent();
        //intent.putExtra(AppConstants.KEY_QR_CODE, rawResult.getText());
        //setResult(RESULT_OK, intent);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getApplicationContext());
        builder.setTitle(code)
                .setMessage(formate)
                .setNeutralButton("OK", null);

        AlertDialog dialog = builder.create();
        dialog.show();
        finish();
    }
}


 */