package com.unswsocietyplus.unsw_society_plus;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by chong on 21/03/2018.
 */

public class homeFragment extends Fragment{
    View myView;
    TextView zidLbl;
    TextView nameLbl;
    TextView zmailLbl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        myView = inflater.inflate(R.layout.home_layout, container,false);
        getActivity().setTitle("Home");
        zidLbl = myView.findViewById(R.id.zidLbl);
        zidLbl.setText(" z" + currentSession.currentUser.zid);
        nameLbl = myView.findViewById(R.id.nameLbl);
        nameLbl.setText(currentSession.currentUser.studentName);
        zmailLbl = myView.findViewById(R.id.zmailLbl);
        zmailLbl.setText(currentSession.currentUser.zmail);

        currentSession.studentEvents.clear();
        for (int i = 0; i < currentSession.currentUser.executiveOf.size(); i++){
            currentSession.loadEvents(currentSession.currentUser.executiveOf.get(i).societyID);
        }
        for (int i = 0; i < currentSession.currentUser.memberOf.size(); i++){
            currentSession.loadEvents(currentSession.currentUser.memberOf.get(i).societyID);
        }
        return myView;
    }


}
