package com.unswsocietyplus.unsw_society_plus;

public class event{
String eventID;
String eventTitle;
String eventDescription;
String date;
String time;
String location;
String docID;
String societyID;
String fbLink;
String address;

public event(){

}
public event (String eventID, String eventTitle, String eventDescription, String date, String time, String location, String societyID){
    this.eventID = eventID;
    this.eventTitle = eventTitle;
    this.eventDescription = eventDescription;
    this.date = date;
    this.time = time;
    this.location = location;
    this.societyID = societyID;
}

public void setEventID(String eventID){
    this.eventID = eventID;
}

public void setEventTitle(String eventTitle){
    this.eventTitle = eventTitle;
}

public void setEventDescription(String eventDescription){
    this.eventDescription = eventDescription;
}

    public void setDate(String date){
        this.date = date;
    }

    public void setLocation(String location){
        this.location = location;
    }

    public void setTime(String time){
        this.time = time;
    }

    public void setDocID(String docID){
        this.docID = docID;
    }

    public void setSocietyID(String societyID){
            this.societyID = societyID;
        }

    public void setFbLink(String fbLink){
    this.fbLink = fbLink;
    }

    public void setAddress(String address){
    this.address = address;
    }
}
