package com.unswsocietyplus.unsw_society_plus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Annoucements extends AppCompatActivity {
    private ListView list;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_annoucements);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });



        // startActivity(new Intent(Vote.this, CreateVote.class));
        list = (ListView) findViewById(R.id.AnnList);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this,R.layout.mylist,arrayList);
        list.setAdapter(adapter);
        registerForContextMenu(list);

        List<String> messages = currentSession.societiesArray;
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // this is disgusting. filthy
        for (String s : currentSession.societiesArray) {
            CollectionReference StudentRef = db.collection("Announcements");
            Query query = StudentRef.whereEqualTo("AnnouncementsSociety", s );

            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            String userName = document.getString("AnnouncementsName");
                            arrayList.add(userName);
                            adapter.notifyDataSetChanged();

                        }
                    } else {
                        System.out.println("DebugErr");

                        Log.d("TAG", "Error getting documents: ", task.getException());
                    }

                }
            });
        }

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.AnnList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.menu3);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }



    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu2);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);


        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // this is disgusting. filthy
        for (String s : currentSession.societiesArray) {
            CollectionReference StudentRef = db.collection("Announcements");
            Query query = StudentRef.whereEqualTo("AnnouncementsName", listItemName );

            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            String userName = document.getString("AnnouncementsDescription");
                            Toast.makeText(getApplicationContext(), userName, Toast.LENGTH_LONG).show();


                        }
                    } else {
                        System.out.println("DebugErr");

                        Log.d("TAG", "Error getting documents: ", task.getException());
                    }

                }
            });
        }

        return false;
    }
    }





