package com.unswsocietyplus.unsw_society_plus;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class editEventActivity extends AppCompatActivity {
    FirebaseFirestore db;
    EditText eventTitle;
    EditText eventDateTime;
    EditText eventDescription;
    EditText location;
    EditText fbLink;
    Button createBtn;
    Button locBtn;
    DatePickerDialog  dp;
    TimePickerDialog mTimePicker;
    SimpleDateFormat df = new SimpleDateFormat("d/M/yyyy");
    SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy_HH:mm");
    int year;
    int month;
    int day;
    String selectedDate;
    boolean gotLoc = false;
    String loc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        setContentView(R.layout.activity_edit_event);
        eventTitle = findViewById(R.id.eventTitleTextF);
        eventDateTime = findViewById(R.id.dateTextF);
        eventDescription = findViewById(R.id.eventDescription);
        location = findViewById(R.id.locationTextF);
        fbLink = findViewById(R.id.fbLink);
        createBtn = findViewById(R.id.createBtn);
        eventDateTime.setKeyListener(null);
        location.setKeyListener(null);
        locBtn = findViewById(R.id.locBtn);



        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                eventDateTime.setText( selectedDate +" "+ selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");

        dp = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                selectedDate = df.format(newDate.getTime());
                eventDateTime.setText(selectedDate);
                mTimePicker.show();
            }}, year,month,day);

        loc = main.viewingEvent.location;
        eventTitle.setText(main.viewingEvent.eventTitle);
        eventDescription.setText(main.viewingEvent.eventDescription);
        eventDateTime.setText(main.viewingEvent.date + " " + main.viewingEvent.time);
        location.setText(main.viewingEvent.address);
        fbLink.setText(main.viewingEvent.fbLink);

    }


    public void editOnClick(View view){
        dp.show();
    }

    public void locOnClick(View view){
        if (!gotLoc){
            try {
                String[] eventLatlng = main.viewingEvent.location.split(",");
                currentSession.selectedLoc = new LatLng(Double.parseDouble(eventLatlng[0]),Double.parseDouble(eventLatlng[1]));
            }catch(Exception ex){
                currentSession.selectedLoc = new LatLng(-33.916597, 151.231689);
            }
            locBtn.setText("Refresh");
            gotLoc = true;
            startActivity(new Intent(editEventActivity.this, MapsActivity.class));

        }else{
            loc = currentSession.selectedLoc.latitude+","+currentSession.selectedLoc.longitude;
            location.setText(currentSession.address);
            locBtn.setText("Edit");
            gotLoc = false;
        }
    }

    public void clearOnclick(View view){
        location.setText("");
        loc = "";
    }


    public void editCancelOnclick(View view){
        finish();

    }

    public void saveOnclick(View view) {
        editEvent();
        finish();
    }


    void editEvent(){

        String[] dateTime = eventDateTime.getText().toString().split(" ");
        Map<String, Object> event = new HashMap<>();
        event.put("EventTitle", eventTitle.getText().toString());
        event.put("EventDescription", eventDescription.getText().toString());
        event.put("Address", location.getText().toString());
        event.put("Location", loc);
        event.put("Date", dateTime[0]);
        event.put("Time", dateTime[1]);
        event.put("fbLink", fbLink.getText().toString());

        db.collection("Events").document(main.viewingEvent.docID)
                .update(event)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("TAG", "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error writing document", e);
                    }
                });

    }








}