package com.unswsocietyplus.unsw_society_plus;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Vote extends AppCompatActivity {
    private ListView list;
    private PopupWindow mPopupWindow;
    private Context mContext;
    boolean check = false;
    private ArrayList<String> Resultslist;
    private HashSet<String> uniqueSet ;
    private RelativeLayout mRelativeLayout;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private String lg ;
    private int lgcount ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       // startActivity(new Intent(Vote.this, CreateVote.class));
        list = (ListView) findViewById(R.id.VoteList);
        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String> (this,R.layout.mylist,arrayList);
        list.setAdapter(adapter);
        registerForContextMenu(list);

      List<String> messages = currentSession.societiesArray;
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // this is disgusting. filthy
        for (String s : messages) {
            CollectionReference StudentRef = db.collection("VoteEvents");
            Query query = StudentRef.whereEqualTo("EventSoceiety", s );

            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            String userName = document.getString("EventName");
                            arrayList.add(userName);
                            adapter.notifyDataSetChanged();

                        }
                    } else {
                        System.out.println("DebugErr");

                        Log.d("TAG", "Error getting documents: ", task.getException());
                    }

                }
            });
        }



    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.VoteList) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.menu);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);

        System.out.println("Debug 1 :" + listItemName);
        CreateVote.VoteOption = listItemName;
        if (menuItemName.equals("Vote") ) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            check = false;
            CollectionReference StudentRef = db.collection("Ballot");
            Query query = StudentRef.whereEqualTo("VoteEventID", listItemName);
            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            if (document.getString("Voter").equals(main.CurrentUser)) {
                                check = true;
                                Toast.makeText(getApplicationContext(), "Sorry, you have already voted.", Toast.LENGTH_LONG).show();
                            }


                        }

                        if (check == false) {
                            startActivity(new Intent(Vote.this, BallotVote.class));
                        }

                    } else {
                        System.out.println("DebugErr");

                        Log.d("TAG", "Error getting documents: ", task.getException());
                    }

                }
            });
            //


        } else {
            /* read it and cry

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("Events").document(listItemName);

            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            String options = document.getString("Options");
                            //List<String> myList = Arrays.asList(options.substring(1, options.length() - 1).replaceAll("\\s", "").split(","));
                           // System.out.println(main.CurrentUser );


                        }
                    } else {
                        System.out.println("DebugErr");
                        Log.d("TAG", "Error getting documents: ", task.getException());
                    }

                }


            }); */
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            Resultslist = new ArrayList<String>();
            uniqueSet = new HashSet<String>();
             lg = "Tie";
             lgcount = 0;

            CollectionReference StudentRef = db.collection("Ballot");
            Query query = StudentRef.whereEqualTo("VoteEventID", listItemName);
            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                        Resultslist.add(document.getString("VoteOption"));
                        uniqueSet.add(document.getString("VoteOption"));
                      // Toast.makeText(getApplicationContext(), document.getString("VoteOption").toString(), Toast.LENGTH_LONG).show();


                            }


                        } else {
                        System.out.println("DebugErr");

                        Log.d("TAG", "Error getting documents: ", task.getException());
                    }
                    for(String key : uniqueSet) {

                        if ( Collections.frequency(Resultslist, key) > lgcount) {
                            lg = key;
                            lgcount = Collections.frequency(Resultslist, key);
                        }
                    }
                    Toast.makeText(getApplicationContext(), ("The current winner is: " + lg + ": " + "with "+ lgcount + " votes" ), Toast.LENGTH_LONG).show();

                }


            });

        }

       // Toast.makeText(Vote.this,
       // System.out.println(menuItemName);
        return true;
    }


    public void CreateBtnClicked(View view) {
        startActivity(new Intent(Vote.this, CreateVote.class));

}

    public static boolean contains(String[] arr, String item) {
        for (String n : arr) {
            if (item.equals(n)) {
                return true;
            }
        }
        return false;
    }



    public static int  ReturnIndex(String[] arr, String item) {
        for (int i = 0; i >= arr.length; i ++) {
            if (arr[i].equals(item)) {
                return i;
            }
        }
        return 0;
    }

}


