package com.unswsocietyplus.unsw_society_plus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class AttendenceScreen extends AppCompatActivity {
    Spinner spinner;
    private ListView list;
    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> adapter2;
    public static String Chosen;
    public static String ChosenLook;

    private ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendence_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        spinner = (Spinner) findViewById(R.id.spinner3) ;
       // ArrayAdapter myAdapter = ((ArrayAdapter) spinner.getAdapter());


       // myAdapter.add(myValue);
      //  myAdapter.notifyDataSetChanged();

        // Fill the spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, currentSession.societiesArray);
        spinner.setAdapter(adapter);
       /* for (int i=0;i < currentSession.societiesArray.size();i++)
        {
            Log.i("Value of element "+i,currentSession.societiesArray.get(i));
            String ob =currentSession.societiesArray.get(i) ;
            myAdapter.add(ob);
            myAdapter.notifyDataSetChanged();
        } */


        // Fill the List
        list = (ListView) findViewById(R.id.killme);
        arrayList = new ArrayList<String>();
        adapter2 = new ArrayAdapter<String> (this,R.layout.mylist,arrayList);
        list.setAdapter(adapter2);
        registerForContextMenu(list);

       // List<String> messages = Arrays.asList("Description", "BITSA", "How", "Are", "Society");
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // this is disgusting. filthy
        for (String s : currentSession.societiesArray) {
            CollectionReference StudentRef = db.collection("Attendance");
            Query query = StudentRef.whereEqualTo("Society", s );

            query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            String userName = document.getString("Name");
                            arrayList.add(userName);
                            adapter2.notifyDataSetChanged();

                        }
                    } else {
                        System.out.println("DebugErr");

                        Log.d("TAG", "Error getting documents: ", task.getException());
                    }

                }
            });
        }

    }

    public void StartBtnClicked(View view) {
        startActivity(new Intent(AttendenceScreen.this, barCode.class));
        Chosen = spinner.getSelectedItem().toString();
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.killme) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.killme);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.killme);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);

        System.out.println("Debug 1 :" + listItemName);
        if (menuItemName.equals("View List") ) {
            ChosenLook = listItemName;

            startActivity(new Intent(AttendenceScreen.this, ViewAttList.class));

        }

        // Toast.makeText(Vote.this,
        // System.out.println(menuItemName);
        return true;
    }
}
