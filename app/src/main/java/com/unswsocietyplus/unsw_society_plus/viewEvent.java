package com.unswsocietyplus.unsw_society_plus;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.net.URL;

public class viewEvent extends AppCompatActivity {
    private TextView title;
    private TextView dateTime;
    private TextView location;
    private TextView description;
    private ImageButton fbBtn;
    private ImageButton mapBtn;
    private Button editBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);
        title = findViewById(R.id.titleLbl);
        dateTime = findViewById(R.id.dateTimeLbl);
        location  = findViewById(R.id.locationLbl);
        description = findViewById(R.id.descriptionLbl);
        fbBtn = findViewById(R.id.fbBtn);
        editBtn = findViewById(R.id.editBtn);
        mapBtn = findViewById(R.id.mapBtn);
        loadEvent();
    }

    public void loadEvent(){
        title.setText(main.viewingEvent.eventTitle);
        dateTime.setText(main.viewingEvent.date + " " + main.viewingEvent.time);
        location.setText(main.viewingEvent.address);
        description.setText(main.viewingEvent.eventDescription);
        if(main.viewingEvent.fbLink!=null){
        if (!main.viewingEvent.fbLink.equals("")) {
            fbBtn.setEnabled(true);
            fbBtn.setImageResource(R.mipmap.fbico);
        }else{
            fbBtn.setEnabled(false);
            fbBtn.setImageResource(R.mipmap.fbgreyico);
        }
    }else{
            fbBtn.setEnabled(false);
            fbBtn.setImageResource(R.mipmap.fbgreyico);
        }



        if(main.viewingEvent.location!=null){
            if (main.viewingEvent.location.contains(",")) {
                mapBtn.setEnabled(true);
                mapBtn.setImageResource(R.mipmap.mapico);
            }else{
                mapBtn.setEnabled(false);
                mapBtn.setImageResource(R.mipmap.greymapico);
            }
        }else{
            mapBtn.setEnabled(false);
            mapBtn.setImageResource(R.mipmap.greymapico);
        }

        if (canEdit(main.viewingEvent)){
            editBtn.setEnabled(true);
        }else{
            editBtn.setEnabled(false);
        }
    }

    public boolean canEdit(event event){
       if (currentSession.currentUser.executiveOf.size()>0) {
           for (int i = 0; i < currentSession.currentUser.executiveOf.size(); i++){
               if (event.societyID.equals(currentSession.currentUser.executiveOf.get(i).societyID)){
                   return true;
               }
           }
       }
       return false;
    }

    public void onClick(View view){
        finish();
    }

    public void editOnclick(View view){
        System.out.println(main.viewingEvent.docID);
        startActivity(new Intent(viewEvent.this, editEventActivity.class));
    }

    public void fbBtnOnclick(View view){
        String url = main.viewingEvent.fbLink;
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);

    }

    public void mapBtnOnclick(View view){

        String url = "https://www.google.com/maps/search/?api=1&query="+main.viewingEvent.location;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

}
