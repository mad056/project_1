package com.unswsocietyplus.unsw_society_plus;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ViewAllSoc extends AppCompatActivity {
    ListView ViewAllList;
    final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";
    public static FirebaseFirestore db;

    private ArrayList<String> Resultslist;
    private HashSet<String> uniqueSet ;
    private ArrayList<String> arrayList;
    private ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_soc);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Load all socieites la


        db = FirebaseFirestore.getInstance();
        Query query = getRef("Societies");
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        society newSociety = new society(document.getString("SocietyID"), document.getString("SocietyName"),
                                document.getString("SocietyDescription"),document.getString("SocietyIconLink"));
                        currentSession.currentUser.AllOf.add(newSociety);
                        //currentSession.loadEvents(newSociety.societyID);
                     //   System.out.println(document.getString("SocietyID") + " added");
                    }
                } else {
                    System.out.println("DebugErr");
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });


        ViewAllList = findViewById(R.id.ViewAllList);
        ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();
        arrayList = new ArrayList<String>();
        for (int i = 0; i < currentSession.currentUser.AllOf.size(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, currentSession.currentUser.AllOf.get(i).societyName);
            item.put(ID_SUBTITLE,currentSession.currentUser.AllOf.get(i).societyID);
            myListData.add(item);
            arrayList.add(currentSession.currentUser.AllOf.get(i).societyName);
        }

        ViewAllList.setAdapter(
                new SimpleAdapter(
                        this,
                        myListData,
                        android.R.layout.simple_list_item_2,
                        new String[]{ID_TITLE, ID_SUBTITLE},
                        new int[]{android.R.id.text1, android.R.id.text2}
                )
        );


        ViewAllList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("position: " + position);
                viewSociety(currentSession.currentUser.executiveOf.get(position));
            }
        });



    }

    public static CollectionReference getRef(String collection){
        return db.collection(collection);
    }



    public void viewSociety(society society){
        main.viewingSociety = society;
        startActivity(new Intent(ViewAllSoc.this, viewSocietyActivity.class));
    }

}
