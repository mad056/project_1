package com.unswsocietyplus.unsw_society_plus;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager fragmentManager = getFragmentManager();
    TextView navZid;
    TextView navStudentName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        navZid = header.findViewById(R.id.navZid);
        navStudentName = header.findViewById(R.id.navStudentName);
        navZid.setText("z" + currentSession.currentUser.zid);
        navStudentName.setText(currentSession.currentUser.studentName);
        switchView(new homeFragment());
        currentSession.currentUser.loadSociety();




    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        fragmentManager = getFragmentManager();

        if (id == R.id.nav_home) {
            switchView(new homeFragment());
        } else if (id == R.id.nav_notisficaitions) {
            startActivity(new Intent(home.this, Annoucements.class));
        } else if (id == R.id.nav_myCalendar) {
            startActivity(new Intent(home.this, myEventsActivity.class));

        } else if (id == R.id.nav_manage) {
            startActivity(new Intent(home.this, createEvent.class));
        } else if (id == R.id.nav_share) {
            startActivity(new Intent(home.this, barCode.class));

        } else if (id == R.id.nav_signOut) {
            currentSession.signout();
            super.onBackPressed();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void switchView(Fragment fragment){
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame
                        , fragment)
                .commit();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {

            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                System.out.println(contents);
            }
            if(resultCode == RESULT_CANCELED){
                //handle cancel
            }
        }
    }



    public void VoteBtnClicked(View view) {
        startActivity(new Intent(home.this, Vote.class));
    }

    public void calBtnClicked(View view) {
        startActivity(new Intent(home.this, myEventsActivity.class));
    }

    public void scanBtnClicked(View view) {
        startActivity(new Intent(home.this, AttendenceScreen.class));
    }

    public void societyBtnClicked(View view){
        startActivity(new Intent(home.this, allSocitiesActivity.class));
    }



}
