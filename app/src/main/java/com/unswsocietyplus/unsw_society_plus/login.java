package com.unswsocietyplus.unsw_society_plus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class login extends AppCompatActivity {


    EditText zidTextF;
    EditText passwordTextF;
    TextView warning;
    boolean found = false;
    CheckBox rememberMeCB;
    static main main = new main();
    private FirebaseAuth mAuth;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    Button loginBtn;
    @Override
    // testing
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        zidTextF = findViewById(R.id.zidTextF);
        passwordTextF = findViewById(R.id.passwordTextF);
        warning = findViewById(R.id.warningLbl);
        rememberMeCB = findViewById(R.id.rememberMeCB);
        loginBtn = findViewById(R.id.loginBtn);
        //main.firebaseTesting();
        mAuth = FirebaseAuth.getInstance();
        try{
            settings = getApplicationContext().getSharedPreferences("saved login", 0);
            editor = settings.edit();
            String username = settings.getString("zid", String.valueOf(0));
            String password = settings.getString("password", String.valueOf(0));
            if (!username.equals("")&&!username.equals("0")&&!password.equals("")&&!password.equals("0")) {
                zidTextF.setText(username);
                passwordTextF.setText(password);
                rememberMeCB.setChecked(true);
                loginBtn.performClick();
            }
        }catch (Exception ex){
            System.out.println(ex);
        }


    }
    public void loginBtnClicked(View view) {
        loginBtn.setText("Loading...");
        loginBtn.setEnabled(false);

        warning.setText("");
        mAuth.signInWithEmailAndPassword(main.zidToZmail(zidTextF.getText().toString()), passwordTextF.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            main.zid = zidTextF.getText().toString();
                            // Sign in success, update UI with the signed-in user's information
                            System.out.println("signInWithEmail:success");
                            //FirebaseUser user = mAuth.getCurrentUser();
                            if (rememberMeCB.isChecked()){
                                editor.putString("zid", zidTextF.getText().toString());
                                editor.putString("password", passwordTextF.getText().toString());
                            }else{
                                editor.putString("zid", "");
                                editor.putString("password", "");
                            }
                            editor.apply();
                            //zidTextF.setText("");
                            passwordTextF.setText("");
                            //rememberMeCB.setChecked(false);

                            FirebaseFirestore db = FirebaseFirestore.getInstance();
                            DocumentReference docRef = db.collection("Student").document(zidTextF.getText().toString());

                            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document = task.getResult();
                                        if (document.exists()) {

                                            main.CurrentUser = document.getString("ZID");
                                            currentSession.currentUser = new student();
                                            currentSession.studentEvents = new ArrayList<event>();
                                            currentSession.currentUser.zid = main.CurrentUser;
                                            currentSession.currentUser.studentName = document.getString("FirstName") + " " + document.getString("LastName");
                                            currentSession.zidToZmail();
                                            currentSession.currentUser.stringExecutiveOf = document.getString("JExecutiveOf");
                                            currentSession.currentUser.stringMemberOf = document.getString("JMemberOf");
                                            String stringMemberOf = document.getString("MemberOf");
                                            String stringExecutiveOf = document.getString("ExecutiveOf");

                                            if (!stringMemberOf.equals(null)) {
                                                currentSession.societiesArray = Arrays.asList(stringMemberOf.substring(1, stringMemberOf.length() - 1).replaceAll("\\s", "").split(","));
                                            }


                                            if (!stringExecutiveOf.equals(null)) {
                                                currentSession.executiveArray = Arrays.asList(stringExecutiveOf.substring(1, stringExecutiveOf.length() - 1).replaceAll("\\s", "").split(","));

                                            }
                                            System.out.println(main.CurrentUser );
                                            currentSession.loadStudents(main.CurrentUser );

                                            startActivity(new Intent(login.this, home.class));

                                        }
                                        loginBtn.setText("Login");
                                        loginBtn.setEnabled(true);
                                    } else {
                                        System.out.println("DebugErr");
                                        warning.setText("User Not Found On UNSW System. Contact IT Support.");

                                        Log.d("TAG", "Error getting documents: ", task.getException());
                                        loginBtn.setText("Login");
                                        loginBtn.setEnabled(true);
                                    }

                                }


                            });


                        } else {
                            // If sign in fails, display a message to the user.
                            System.out.println("signInWithEmail:failure");
                            warning.setText("Incorrect zid or password");
                            loginBtn.setText("Login");
                            loginBtn.setEnabled(true);
                        }


                    }


                });



 /*       System.out.println("clicked");
        System.out.println("zidTextF.getText().toString() " + zidTextF.getText().toString());
        System.out.println("passwordTextF.getText().toString() " + passwordTextF.getText().toString());
        if (main.login(zidTextF.getText().toString(), passwordTextF.getText().toString())) {
            System.out.println("success");
        } else {
            System.out.println("fail");
        }
         */

    }
}


