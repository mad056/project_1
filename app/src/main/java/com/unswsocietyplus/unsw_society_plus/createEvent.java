package com.unswsocietyplus.unsw_society_plus;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class createEvent extends AppCompatActivity {
    FirebaseFirestore db;
    Button locBtn;
    EditText eventTitle;
    EditText eventDateTime;
    EditText fbLink;
    EditText eventDescription;
    EditText location;
    Button createBtn;
    DatePickerDialog  dp;
    TimePickerDialog mTimePicker;
    Spinner spinner;
    boolean gotLoc = false;
    SimpleDateFormat df = new SimpleDateFormat("d/M/yyyy");
    SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy_HH:mm");
    int year;
    int month;
    int day;
    String selectedDate;
    String loc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        setContentView(R.layout.activity_create_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        eventTitle = findViewById(R.id.eventTitleTextF);
        eventDateTime = findViewById(R.id.dateTextF);
        eventDescription = findViewById(R.id.eventDescription);
        location = findViewById(R.id.locationTextF);
        createBtn = findViewById(R.id.createBtn);
        fbLink = findViewById(R.id.fbLink);
        eventDateTime.setKeyListener(null);
        location.setKeyListener(null);
        spinner = findViewById(R.id.spinner);
        locBtn = findViewById(R.id.locBtn);


        Calendar cal = Calendar.getInstance();
        year = cal.get(Calendar.YEAR);
        month = cal.get(Calendar.MONTH);
        day = cal.get(Calendar.DAY_OF_MONTH);
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                eventDateTime.setText( selectedDate +" "+ selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");

        dp = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                selectedDate = df.format(newDate.getTime());
                eventDateTime.setText(selectedDate);
                mTimePicker.show();
            }}, year,month,day);
        String[] stringArray = new String[currentSession.currentUser.executiveOf.size()];
        for (int i = 0; i < stringArray.length; i++){
            stringArray[i] = currentSession.currentUser.executiveOf.get(i).societyID;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, stringArray);
        spinner.setAdapter(adapter);


    }


    public void editOnClick(View view){

        dp.show();

    }


    public void createButtonClicked(View view) {
        createEvent();
        finish();
    }
    //testing
public void locOnClick(View view){
        if (!gotLoc){
            locBtn.setText("Refresh");
            gotLoc = true;
            startActivity(new Intent(createEvent.this, MapsActivity.class));

        }else{
            loc = currentSession.selectedLoc.latitude+","+currentSession.selectedLoc.longitude;
            location.setText(currentSession.address);
            locBtn.setText("Edit");
            gotLoc = false;
        }
}

public void clearOnclick(View view){
        location.setText("");
}


    void createEvent(){

        String[] dateTime = eventDateTime.getText().toString().split(" ");
        Map<String, Object> event = new HashMap<>();
        event.put("EventTitle", eventTitle.getText().toString());
        event.put("EventDescription", eventDescription.getText().toString());
        event.put("Address", location.getText().toString());
        event.put("Location", loc);
        event.put("Date", dateTime[0]);
        event.put("Time", dateTime[1]);
        event.put("Society", spinner.getSelectedItem());
        event.put("fbLink", fbLink.getText().toString());

        db.collection("Events")
                .add(event)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("TAG", "DocumentSnapshot written with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TAG", "Error writing document", e);
                        //noActionsAlert("Event creation failed", "Errors occurred, please make sure that all necessary information is entered");
                    }
                });

    }


    public void cancelOnclick(View view){
        finish();
    }



}