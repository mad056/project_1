package com.unswsocietyplus.unsw_society_plus;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ViewAttList extends AppCompatActivity {
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_att_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        FirebaseFirestore db = FirebaseFirestore.getInstance();

        // Declare the textviews

        list = (ListView) findViewById(R.id.Metoo);

        arrayList = new ArrayList<String>();
        adapter = new ArrayAdapter<String>(this, R.layout.mylist, arrayList);
        list.setAdapter(adapter);

        // for (String s : messages) {
        CollectionReference StudentRef = db.collection("Attendance");
        DocumentReference docRef = db.collection("Attendance").document(AttendenceScreen.ChosenLook);

        //      Query query = StudentRef.whereEqualTo("VoteName", CreateVote.VoteOption );
        System.out.println("Debug 2 :" + CreateVote.VoteOption);

        //    query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

            @Override
            //      public void onComplete(@NonNull Task<QuerySnapshot> task) {
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (task.isSuccessful()) {


                    //   for (DocumentSnapshot document : task.getResult()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {

                        String options = document.getString("List");
                        List<String> myList = Arrays.asList(options.substring(1, options.length() - 1).replaceAll("\\s", "").split(","));
                        for (String obj : myList) {
                            arrayList.add(obj);
                            adapter.notifyDataSetChanged();
                        }


                    }
                } else {
                    System.out.println("DebugErr");

                    Log.d("TAG", "Error getting documents: ", task.getException());
                }

            }
        });
    }

}
