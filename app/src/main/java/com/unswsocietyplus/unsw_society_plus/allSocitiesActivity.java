package com.unswsocietyplus.unsw_society_plus;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;


public class allSocitiesActivity extends AppCompatActivity {
    ListView executiveListView;
    ListView memberListView;
    final String ID_TITLE = "TITLE", ID_SUBTITLE = "SUBTITLE";

    boolean check = false;
    private ArrayList<String> Resultslist;
    private HashSet<String> uniqueSet ;
    private ArrayList<String> arrayList;
    private ArrayAdapter<String> adapter;
    private int lgcount ;
public static String ChosenAnn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_societies);
        executiveListView = findViewById(R.id.executiveListView);
        memberListView = findViewById(R.id.memberListView);
        System.out.println("currentSession.currentUser.executiveOf.size() " + currentSession.currentUser.executiveOf.size());
        System.out.println("currentSession.currentUser.memberOf.size() " + currentSession.currentUser.memberOf.size());

        ArrayList<HashMap<String, String>> myListData = new ArrayList<HashMap<String, String>>();
        arrayList = new ArrayList<String>();
        for (int i = 0; i < currentSession.currentUser.executiveOf.size(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, currentSession.currentUser.executiveOf.get(i).societyName);
            item.put(ID_SUBTITLE,currentSession.currentUser.executiveOf.get(i).societyID);
            myListData.add(item);
            arrayList.add(currentSession.currentUser.executiveOf.get(i).societyName);
        }


        executiveListView.setAdapter(
                new SimpleAdapter(
                        this,
                        myListData,
                        android.R.layout.simple_list_item_2,
                        new String[]{ID_TITLE, ID_SUBTITLE},
                        new int[]{android.R.id.text1, android.R.id.text2}
                )
        );

        executiveListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("position: " + position);
                viewSociety(currentSession.currentUser.executiveOf.get(position));
            }
        });

        ArrayList<HashMap<String, String>> myListData2 = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < currentSession.currentUser.memberOf.size(); i++) {
            HashMap<String, String> item = new HashMap<String, String>();
            item.put(ID_TITLE, currentSession.currentUser.memberOf.get(i).societyName);
            item.put(ID_SUBTITLE,currentSession.currentUser.memberOf.get(i).societyID);
            myListData2.add(item);
        }

        registerForContextMenu(executiveListView);

        memberListView.setAdapter(
                new SimpleAdapter(
                        this,
                        myListData2,
                        android.R.layout.simple_list_item_2,
                        new String[]{ID_TITLE, ID_SUBTITLE},
                        new int[]{android.R.id.text1, android.R.id.text2}

                )
        );

      /*  if ( executiveListView.getAdapter().getCount() == 0) {
            for (int i = 0; i < currentSession.executiveArray.size(); i++) {
                HashMap<String, String> item = new HashMap<String, String>();
                item.put(ID_TITLE, currentSession.executiveArray.get(i));
                item.put(ID_SUBTITLE,currentSession.executiveArray.get(i));
                myListData.add(item);
            }
        }
        if ( memberListView.getAdapter().getCount() == 0) {
            for (int i = 0; i < currentSession.societiesArray.size(); i++) {
                HashMap<String, String> item = new HashMap<String, String>();
                item.put(ID_TITLE, currentSession.societiesArray.get(i));
                item.put(ID_SUBTITLE, currentSession.societiesArray.get(i));
                myListData2.add(item);
            }
        }*/

        memberListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("position: " + position);
                viewSociety(currentSession.currentUser.memberOf.get(position));
            }
        });





    }



    public void OnViewall(View view) {
        startActivity(new Intent(allSocitiesActivity.this, ViewAllSoc.class));
    }

    public void viewSociety(society society){
        main.viewingSociety = society;
        startActivity(new Intent(allSocitiesActivity.this, viewSocietyActivity.class));
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.executiveListView) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            menu.setHeaderTitle(arrayList.get(info.position));
            String[] menuItems = getResources().getStringArray(R.array.menu2);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }



    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu2);
        String menuItemName = menuItems[menuItemIndex];
        String listItemName = arrayList.get(info.position);




        System.out.println("Debug 1 :" + listItemName);
        if (menuItemName.equals("Create Announcement")) {

            for (int i = 0; i < currentSession.currentUser.executiveOf.size(); i++) {
                if (currentSession.currentUser.executiveOf.get(i).societyName.equals(listItemName) ) {
                    ChosenAnn = currentSession.currentUser.executiveOf.get(i).societyID;
                }
            }
            startActivity(new Intent(allSocitiesActivity.this, CreateAnnoucement.class));

        }
        return true;
    }

}
