package com.unswsocietyplus.unsw_society_plus;


import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

/**
 * Created by Joson on 21/04/2018.
 */

class student {
    String zid;
    String studentName;
    String zmail;
    String stringMemberOf;
    String stringExecutiveOf;
    ArrayList<society> memberOf = new ArrayList<>();
    ArrayList<society> AllOf = new ArrayList<>();

    ArrayList<society> executiveOf = new ArrayList<>();
    public static FirebaseFirestore db;
    public static CollectionReference getRef(String collection){
        return db.collection(collection);
    }


    student(){
    }

    student(String zid, String studentName){
        this.zid = zid;
        this.studentName = studentName;
        zmail = "z" + zid + "@student.unsw.edu.au";
    }


    public void loadSociety(){

        if (!stringExecutiveOf.equals("")) {
                System.out.println("stringExecutiveOf: " + stringExecutiveOf);
            if (stringExecutiveOf.contains(",")){
                String[] stringArrayExecutive = stringExecutiveOf.split(",");
                for (int i =0; i<stringArrayExecutive.length; i++){
                    loadSocietyAsExecutive(stringArrayExecutive[i]);
                    }}else{
                loadSocietyAsExecutive(stringExecutiveOf);
            }
        }

        if (!stringMemberOf.equals("")) {
            if (stringMemberOf.contains(",")){
                System.out.println("stringMemberOf: " + stringMemberOf);
                String[] stringArrayMember = stringMemberOf.split(",");
                for (int i = 0; i < stringArrayMember.length; i++) {
                    loadSocietyAsMember(stringArrayMember[i]);
                }
            }else{
                loadSocietyAsMember(stringMemberOf);
            }
        }
    }



    public void loadSocietyAsExecutive(String code){
        db = FirebaseFirestore.getInstance();
        Query query = getRef("Societies").whereEqualTo("SocietyID", code);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        society newSociety = new society(document.getString("SocietyID"), document.getString("SocietyName"),
                                document.getString("SocietyDescription"),document.getString("SocietyIconLink"));
                        currentSession.currentUser.executiveOf.add(newSociety);
                        currentSession.loadEvents(newSociety.societyID);
                        System.out.println(document.getString("SocietyID") + " added");
                    }
                } else {
                    System.out.println("DebugErr");
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });
    }

    public void loadSocietyAsMember(String code){
        db = FirebaseFirestore.getInstance();
        Query query = getRef("Societies").whereEqualTo("SocietyID", code);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        society newSociety = new society(document.getString("SocietyID"), document.getString("SocietyName"),
                                document.getString("SocietyDescription"),document.getString("SocietyIconLink"));
                        currentSession.currentUser.memberOf.add(newSociety);
                        currentSession.loadEvents(newSociety.societyID);
                        System.out.println(document.getString("SocietyID") + " added");
                    }
                } else {
                    System.out.println("DebugErr");
                    Log.d("TAG", "Error getting documents: ", task.getException());
                }
            }
        });
    }
}
